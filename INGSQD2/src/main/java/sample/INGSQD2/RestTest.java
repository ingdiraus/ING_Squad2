package sample.INGSQD2;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestTest {
	
	@Test
	public void getStockName(){
		RestAssured.baseURI= "http://restapi.demoqa.com/utilities/weather/city";
		//RestAssured.baseURI= "http://localhost:8090/getstocks";
		
		RequestSpecification httpRequest = RestAssured.given();
		
		Response response = httpRequest.request(Method.GET, "/Hyderabad");
		//Response response = httpRequest.request(Method.GET, "/Symbol");
		
		String responseBody = response.getBody().asString();
		System.out.println(responseBody);
		
		JsonPath jsonPathElevator =  response.jsonPath();
		String city = jsonPathElevator.getString("City");
		Assert.assertEquals(city, "Hyderabad");
		
		String temperature = jsonPathElevator.getString("Temperature");
		Assert.assertEquals(temperature, "39 Degree celsius");
		
		String humidity = jsonPathElevator.getString("Humidity");
		Assert.assertEquals(humidity, "20 Percent");
		
		int statusCode = response.getStatusCode();
		System.out.println("Status code received is " + statusCode);
		Assert.assertEquals(statusCode, 200, "Status code returned is correct");
	}

}
