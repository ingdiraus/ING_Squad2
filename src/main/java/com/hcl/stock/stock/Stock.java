package com.hcl.stock.stock;

public class Stock {

	String symbol;
	Double price;
	
	public Stock(String symbol, double price) {
		super();
		this.symbol = symbol;
		this.price = price;
	}
	
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
}
