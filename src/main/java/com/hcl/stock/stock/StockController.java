package com.hcl.stock.stock;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StockController {
	
	List<Stock> stocks = new ArrayList<>();
	
	@RequestMapping("/getStocks")
	public List<Stock> getStocks(){
		stocks = getMockStocks();
		return stocks;
	}


	private List<Stock> getMockStocks() {
		List<Stock> mockStocks = new ArrayList<>();
		mockStocks.add(new Stock("HCL",905.5d));
		mockStocks.add(new Stock("INFY",1000.0d));
		mockStocks.add(new Stock("HEXW",200d));
		mockStocks.add(new Stock("GENPA",30d));
		mockStocks.add(new Stock("EKA",305.75d));
		
		return mockStocks;
	}

}
