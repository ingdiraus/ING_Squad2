﻿@UI
Feature: CompanyStockPurchase
	As an end user I want to select a company stock and buy its stock

@BuyStock-HappyFlow
Scenario Outline: BuyCompanyStock
	Given I am on the homepage<UrlToLaunch> of the stock page
	When I select a user from the home page
	Then navigates to the stock selection page
	When I select a company <CompanyName>
	Then I should land on Purchase page
	When I enter number of stocks to buy
	Then the value to pay for the stock is displayed
	When I Click on confirm button
	Then Successful message should be displayed<SuccessMessage>
	Examples:
	| UrlToLaunch                                                                                | CompanyName | SuccessMessage               |
	| 10.117.189.27:8000 | HCL         | Stock purchased successfully |


@ValidateFunctionalityToChooseAnotherCompany
Scenario Outline: BuyCompanyStock-BackButtonValidation
	Given I am on the homepage<UrlToLaunch> of the stock page
	When I select a user from the home page
	Then navigates to the stock selection page
	When I select a company <CompanyName>
	Then I should land on Purchase page
	When I Click on the back button on the page
	Then I should land on the selection page again
	When I select another company
	Then I should land on Purchase page
	Examples:
	| UrlToLaunch                  | CompanyName | SuccessMessage               |
	| https://www.moneycontrol.com | Wipro       | Stock purchased successfully |
