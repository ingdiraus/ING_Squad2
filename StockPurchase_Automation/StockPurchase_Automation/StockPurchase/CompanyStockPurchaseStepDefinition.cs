﻿using System;
using TechTalk.SpecFlow;

namespace StockPurchase_Automation
{
    [Binding]
    public class CompanyStockPurchaseStepDefinition
    {
        [Given(@"I am on the homepage(.*) of the stock page")]
        public void GivenIAmOnTheHomepageOfTheStockPage(string pageUrl)
        {
            var pgObj = new Events();
            pgObj.openBroswer(pageUrl);
            // -- call the method to initiate the driver and launch the browser
        }

        [When(@"I select a user from the home page")]
        public void WhenISelectAUserFromTheHomePage()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"navigates to the stock selection page")]
        public void ThenNavigatesToTheStockSelectionPage()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I select a company (.*)")]
        public void WhenISelectACompany(string selectCompany)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I should land on Purchase page")]
        public void ThenIShouldLandOnPurchasePage()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I enter number of stocks to buy")]
        public void WhenIEnterNumberOfStocksToBuy()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"the value to pay for the stock is displayed")]
        public void ThenTheValueToPayForTheStockIsDisplayed()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I Click on confirm button")]
        public void WhenIClickOnConfirmButton()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"Successful message should be displayedStock purchased successfully")]
        public void ThenSuccessfulMessageShouldBeDisplayedStockPurchasedSuccessfully()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I Click on the back button on the page")]
        public void WhenIClickOnTheBackButtonOnThePage()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I should land on the selection page again")]
        public void ThenIShouldLandOnTheSelectionPageAgain()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I select another company")]
        public void WhenISelectAnotherCompany()
        {
            ScenarioContext.Current.Pending();
        }

    }
}
