﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace StockPurchase_Automation
{
    public class Events : IEvents
    {
        IWebDriver driver = new ChromeDriver();
        public void openBroswer(string url)
        {
            driver.Navigate().GoToUrl(url);
            driver.Manage().Window.Maximize();
        }

        public void clickOnElement(string elementID)
        {
            driver.FindElement(By.Id(elementID)).Click();;
        }

        public void closeBroswer()
        {
            driver.Quit();
        }

        public void enterText(string elementID, string text)
        {
            driver.FindElement(By.Id(elementID)).SendKeys(text);
        }

        public string getTitle()
        {
            return driver.Title;
        }

        public void selectDropdown(string elementID, string visibleText)
        {
            SelectElement select = new SelectElement(driver.FindElement(By.Id(elementID)));
            select.SelectByText(visibleText);

        }
    }
}
