﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockPurchase_Automation
{
    public interface IEvents
    {
        void openBroswer(string url);
        void selectDropdown(string elementID, string visibleText);
        void clickOnElement(string elementID);
        void enterText(string elementID, string text);
        void closeBroswer();
        string getTitle();

    }

}
