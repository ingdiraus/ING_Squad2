﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using System;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace StockPurchase_Automation.Reporting
{
    [TestFixture]
    public class TestReport
    {
        //Instance of extents reports
        public static ExtentReports extent;
        public static ExtentTest test;
        
        [OneTimeSetUp]
        public void StartReport()
        {

            //To obtain the current solution path/project path

            string pth = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string actualPath = pth.Substring(0, pth.LastIndexOf("bin"));
            string projectPath = new Uri(actualPath).LocalPath;

            //Append the html report file to current project path
            string reportPath = projectPath + "Reports\\TestRunReport.html";
            //extent = new ExtentReports(reportPath, true);
            //extent = new ExtentReports(reportPath, true);
            
            
            //extent.LoadConf(projectPath + "Extent-Config.xml"); 
        }

        [Test]
        public void ReportPass()
        {
            //test = extent.
        }



        [TearDown]
        public void AfterClass()
        {
            //StackTrace details for failed Testcases
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stackTrace = "+ TestContext.CurrentContext.Result.StackTrace + ";
            var errorMessage = TestContext.CurrentContext.Result.Message;
            //if (status == TestStatus.Failed)
            //{
            //    test.Log(LogStatus.Fail, status + errorMessage);
            //}
            ////End test report
            //extent.EndTest(test);
            //driver.Quit();
        }

        [OneTimeTearDown]
        public void EndReport()
        {
            //End Report
            extent.Flush();
            //extent.Close();
        }
    }
}
